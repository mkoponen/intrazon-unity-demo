# Instructions for developers

IntraZon consists of two major parts. The one is the API, which is a JSON-based REST API and for which plugins are provided by Alpenwolf for certain
languages and frameworks to simplify its use. If the developer so chooses, the raw REST API can always be used instead.

The other part is a control panel, which is currently found at http://devel.intrazon.com/panel/ and when we open up the production environment, at
https://iz.alpenwolf.com . Production environment will only accept HTTPS-connections. This Unity plugin will handle HTTPS automatically when
communicating with production environment.

## Using the plugin in Unity applications

This project contains a directory called Intrazon, and the developer is not expected to change its contents. The plugin is used by dropping this
directory with its contents into the project. The contents of "plugins" and "Resources" are to be added to the corresponding directories of the project
that uses Intrazon. Currently the only third-party plugin required by this IntraZon plugin, is SimpleJSON which is found in the plugins-directory.

The only file in the root directory, GameLogicManager.cs, is an extremely simple skeleton code example for the real application's corresponding, game
logic handling object. It doesn't need to have that exact name, it merely needs to contain all functionality of the example. The most important task is
to launch IzStoreManager -object into the scene. This is a singleton object (there is always precisely one of them running at all times) which handles
all the REST API operations while IntraZon is processing. For example, the object might first submit credit card information, then purchase coins, and
then purchase a virtual good, when the developer merely requested a virtual good to be purchased. Or, if the credit card info was already on file and
player had adequate coins, only the last step will happen. IzStoreManager will then call the requested callback function, when the operation finishes,
successfully or not. From the developer's perspective, one API function was called, and eventually a callback function got called with the result.
There can be a few seconds' delay between these two events.

Note the LoggedIn -function in the GameLogicManager example code. It is currently set to load the store scene immediately upon successful login. In a
real application you would load the game scene instead, but when you wish to display the store, you will need to do what the example LoggedIn -function
does, namely, call the IzStoreManager's SetCanvasActive -method, with the parameter string "store". This will destroy all existing objects from your
scene, and load a new scene. When you use the store this way, you will save your game state by serializing it, and load it back after the player is
finished using the store. However, if you don't already have load/save functionality or it's too heavy and slow, you also have the option of loading
the store scene additively, by giving true as second parameter to the function. See the comments in the code for details. In short, you are responsible
for making your own GameObjects invisible, and in the end, calling DestroyAllSceneObjects in IzStoreManager. If you loaded destructively, then simply
load your own scene again destructively, and set your objects to the deserialized game state you saved.

IntraZon goods are purchased with coins. The value of each coin is USD 0.20 and the minimum number of coins that can be purchased in a single credit
card transaction is three coins. The plugin will automatically note if the player does not have enough coins to purchase the chosen good. In this
case, more coins, but no less than three, will be purchased. If this is the first time this player is using his credit card, a form for entering the
credit card info will be shown, then coins will be purchased, and then the virtual good will be purchased. For example, if the player had two coins and
the good cost four coins, the minimum three coins will be purchased so that the player now has five coins. Then the virtual good cost will be deducted,
which leaves the player with one coin to use for a future purchase. If the player had adequate coins, the purchase is immediate and credit card will
not be charged.

The example project will never leave the store, as there is no scene to go to. In a real application, the application's game logic would be waiting for
events to the provided delegate functions, and know when to return to the actual game scene.

Currently the visual look of the store is extremely basic. It will be updated aggressively within the next few days. None of the API-functions will
change, so in order to update the store to the newest version, simply drop in the new files to Intrazon directory.

## Using the IntraZon control panel

New applications, and their virtual and digital goods, are created in this control panel. You will then receive the unique keys that refer to your new
application, which the IzStoreManager initialization function requires.

Instructions for this will be added within a few hours from this commit.
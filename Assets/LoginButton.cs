﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using SimpleJSON;

public class LoginButton : MonoBehaviour {

	[SerializeField]
	public string id = "";
	[SerializeField] private Button MyButton = null; // assign in the editor

	// Use this for initialization
	void Start () {
		MyButton = this.transform.GetComponent<Button> ();
		MyButton.onClick.AddListener(() => Pushed());
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Pushed() {

		GameObject gameLogicManager = GameObject.Find ("GameLogicManager");
		GameLogicManager manager = gameLogicManager.transform.GetComponent<GameLogicManager> ();
		manager.LoginPlayer ();
	}
}

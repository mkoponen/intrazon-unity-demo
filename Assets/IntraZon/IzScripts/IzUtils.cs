﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class IzUtils {

	public static int getResponseCode(WWW request) {
		int ret = 0;
		if (request.responseHeaders == null) {
			Debug.LogError("no response headers.");
		} else {
			if (!request.responseHeaders.ContainsKey("STATUS")) {
				Debug.LogError("response headers has no STATUS.");
			} else {
				ret = parseResponseCode(request.responseHeaders["STATUS"]);
			}
		}
		return ret;
	}

	public static int parseResponseCode(string statusLine) {
		int ret = 0;

		string[] components = statusLine.Split(' ');
		if (components.Length < 3) {
			Debug.LogError("invalid response status: " + statusLine);
		} else {
			if (!int.TryParse(components[1], out ret)) {
				Debug.LogError("invalid response code: " + components[1]);
			}
		}
		return ret;
	}

	/// <summary>
	/// Sets the data fields of the "player" parameter with data from IntraZon based on given site and username. If this fails, 
	/// sets the key-field of player to null; check this first!
	/// </summary>
	public static IEnumerator GetPlayerWithUsername(string iz_host, string site_auth_key, string site_key, 
		string username, IzPlayer player) {
		string url_format_getplayer_site_username = "/api/v1/sites/{0}/players-uname/{1}/";
		string url_getplayer_username = iz_host + String.Format (url_format_getplayer_site_username, site_key, username);
		// Now we have the URL. We fetch a JSON string representing this player.
		WWW www = IzUtils.CreateWWW (url_getplayer_username, site_auth_key);
		yield return www;

		if (!string.IsNullOrEmpty (www.error)) {
			player.key = null;
		} else {
			_IzPlayer receivedPlayer = JsonUtility.FromJson<_IzPlayer> (www.text);

			player.key = receivedPlayer.key;
			player.email = receivedPlayer.email;
			player.payment_method_filed = receivedPlayer.payment_method_filed;
			player.username = username;
			player.coins = receivedPlayer.coins;
		}
	}

	/// <summary>
	/// Buys coins for player, if credit card has been filed.
	/// </summary>
	public static IEnumerator BuyCoins(string iz_host, string site_auth_key, string site_key, string player_key, int num_coins, IzCoroutineError error) {

		string url_format_paymentmethod = "/api/v1/sites/{0}/players/{1}/buy-coins/";
		string url_paymentmethod = iz_host + String.Format(url_format_paymentmethod, site_key, player_key);

		// We do a simple HTTP GET to the URL.
		_IzBuyCoinsCmd cmd = new _IzBuyCoinsCmd();
		cmd.coins = num_coins;
		WWW www = IzUtils.CreateWWW (url_paymentmethod, site_auth_key, "POST", JsonUtility.ToJson(cmd));
		yield return www;
		int responseCode = getResponseCode (www);
		if (responseCode == 0) {

			error.success = false;
			error.error_message = "Unknown error: Failed to get response code.";
			yield break;

		} else if (responseCode != 200) {

			error.success = false;
			error.error_message = "Response code: " + responseCode;
			yield break;

		} else {

			IzServerResponse response;
			try {
				response = JsonUtility.FromJson<IzServerResponse> (www.text);
			} 
			catch {

				error.success = false;
				error.error_message = "IntraZon internal error.";
				yield break;
			}
			if (response.code != 0) {

				error.success = false;
				error.error_message = "Error from IntraZon: " + response.message;
				yield break;
			}
		}
		error.success = true;
	}

	/// <summary>
	/// Buys virtual good for player, if player has enough coins.
	/// </summary>
	public static IEnumerator BuyVirtualGood(string iz_host, string app_auth_key, string site_key, string app_key, string player_key, string virtual_good_key, IzCoroutineError error) {

		string url_format_buy = "/api/v1/sites/{0}/applications/{1}/players/{2}/buy-virtual-good/";
		string url_buy = iz_host + String.Format(url_format_buy, site_key, app_key, player_key);

		// We do a simple HTTP GET to the URL.
		_IzBuyVirtualGoodCmd cmd = new _IzBuyVirtualGoodCmd();
		cmd.currency = "coins";
		cmd.virtual_good_key = virtual_good_key;
		WWW www = IzUtils.CreateWWW (url_buy, app_auth_key, "POST", JsonUtility.ToJson(cmd));
		yield return www;
		int responseCode = getResponseCode (www);
		if (responseCode == 0) {

			error.success = false;
			error.error_message = "Unknown error: Failed to get response code.";
			yield break;

		} else if (responseCode != 200) {

			error.success = false;
			error.error_message = "Response code: " + responseCode;
			yield break;

		} else {

			IzServerResponse response;
			try {
				response = JsonUtility.FromJson<IzServerResponse> (www.text);
			} 
			catch {

				error.success = false;
				error.error_message = "IntraZon internal error.";
				yield break;
			}
			if (response.code != 0) {

				error.success = false;
				error.error_message = "Error from IntraZon: " + response.message;
				yield break;
			}
		}
		error.success = true;
	}

	/// <summary>
	/// Consumes one PERMANENT virtual good from player. For non-permanent one-time immediate-use goods, do not use or IZ will give status 400 to signify that player doesn't have the virtual good.
	/// </summary>
	public static IEnumerator RemoveVirtualGood(string iz_host, string app_auth_key, string site_key, string app_key, string player_key, string virtual_good_key, IzCoroutineError error) {

		string url_format_remove = "/api/v1/sites/{0}/applications/{1}/players/{2}/remove-virtual-good/";
		string url_remove = iz_host + String.Format(url_format_remove, site_key, app_key, player_key);

		// We do a simple HTTP GET to the URL.
		_IzRemoveVirtualGoodCmd cmd = new _IzRemoveVirtualGoodCmd();
		cmd.virtual_good_key = virtual_good_key;
		WWW www = IzUtils.CreateWWW (url_remove, app_auth_key, "POST", JsonUtility.ToJson(cmd));
		yield return www;
		int responseCode = getResponseCode (www);
		if (responseCode == 0) {

			error.success = false;
			error.error_message = "Unknown error: Failed to get response code.";
			yield break;

		} else if (responseCode != 200) {

			error.success = false;
			error.error_message = "Response code: " + responseCode;
			yield break;

		} else {

			IzServerResponse response;
			try {
				response = JsonUtility.FromJson<IzServerResponse> (www.text);
			} 
			catch {

				error.success = false;
				error.error_message = "IntraZon internal error.";
				yield break;
			}
			if (response.code != 0) {

				error.success = false;
				error.error_message = "Error from IntraZon: " + response.message;
				yield break;
			}
		}
		error.success = true;
	}

	/// <summary>
	/// Buys downloadable good for player, if player has enough coins.
	/// </summary>
	public static IEnumerator BuyDownloadableGood(string iz_host, string app_auth_key, string site_key, string app_key, string player_key, string downloadable_good_key, IzCoroutineError error) {

		string url_format_buy = "/api/v1/sites/{0}/applications/{1}/players/{2}/buy-downloadable-good/";
		string url_buy = iz_host + String.Format(url_format_buy, site_key, app_key, player_key);

		// We do a simple HTTP GET to the URL.
		_IzBuyDownloadableGoodCmd cmd = new _IzBuyDownloadableGoodCmd();
		cmd.currency = "coins";
		cmd.downloadable_good_key = downloadable_good_key;
		WWW www = IzUtils.CreateWWW (url_buy, app_auth_key, "POST", JsonUtility.ToJson(cmd));
		yield return www;
		int responseCode = getResponseCode (www);
		if (responseCode == 0) {

			error.success = false;
			error.error_message = "Unknown error: Failed to get response code.";
			yield break;

		} else if (responseCode != 200) {

			error.success = false;
			error.error_message = "Response code: " + responseCode;
			yield break;

		} else {

			IzServerResponse response;
			try {
				response = JsonUtility.FromJson<IzServerResponse> (www.text);
			} 
			catch {

				error.success = false;
				error.error_message = "IntraZon internal error.";
				yield break;
			}
			if (response.code != 0) {

				error.success = false;
				error.error_message = "Error from IntraZon: " + response.message;
				yield break;
			}
		}
		error.success = true;
	}

	/// <summary>
	/// Logs a player in to IntraZon based on given player key.
	/// </summary>
	public static IEnumerator LoginPlayer(string iz_host, string app_auth_key, string site_key, string app_key, string player_key) {

		string url_format_player = iz_host + "/api/v1/sites/{0}/applications/{1}/players/{2}/login/";
		string player_url = String.Format (url_format_player, site_key, app_key, player_key);

		// We are only changing one field, so this sends minimum amount of data. We could also have fetched the entire player object, 
		// changed the field, and written it back, but that is inefficient.
		WWW www = IzUtils.CreateWWW (player_url, app_auth_key);
		yield return www;
	}

	/// <summary>
	/// Files credit card info for the given player.
	/// </summary>
	public static IEnumerator FileCreditCard(string iz_host, string site_auth_key, string site_key, string player_key, IzCreditCard creditCard, IzCoroutineError error) {

		string url_format_paymentmethod = "/api/v1/sites/{0}/players/{1}/payment-method/";
		string url_paymentmethod = iz_host + String.Format(url_format_paymentmethod, site_key, player_key);

		_IzCreditCard _creditCard = new _IzCreditCard ();
		_creditCard.cc_csc = creditCard.cc_csc;
		_creditCard.cc_exp_month = creditCard.cc_exp_month;
		_creditCard.cc_exp_year = creditCard.cc_exp_year;
		_creditCard.cc_name = creditCard.cc_name;
		_creditCard.cc_num = creditCard.cc_num;

		// We do a simple HTTP GET to the URL.
		WWW www = IzUtils.CreateWWW (url_paymentmethod, site_auth_key, "POST", JsonUtility.ToJson(_creditCard));
		yield return www;
		int responseCode = getResponseCode (www);
		if (responseCode == 0) {
			
			error.success = false;
			error.error_message = "Unknown error: Failed to get response code.";
			yield break;

		} else if (responseCode != 200) {
			
			error.success = false;
			error.error_message = "Response code: " + responseCode;
			yield break;

		} else {
			
			IzServerResponse response;
			try {
				response = JsonUtility.FromJson<IzServerResponse> (www.text);
			} 
			catch {
				
				error.success = false;
				error.error_message = "IntraZon internal error.";
				yield break;
			}
			if (response.code != 0) {
				
				error.success = false;
				error.error_message = "Error from IntraZon: " + response.message;
				yield break;
			}
		}
		error.success = true;
	}

	/// <summary>
	/// Fills the given list virtual_goods with store contents for this app; with objects of type IzVirtualGood.
	/// </summary>
	public static IEnumerator GetVirtualGoods(string iz_host, string app_auth_key, string app_key, List<IzStoreVirtualGood> virtual_goods, 
		IzCoroutineError error) {

		string url_format_virtualgoods = "/api/v1/applications/{0}/virtual-goods/";
		string url_virtualgoods = iz_host + String.Format(url_format_virtualgoods, app_key);

		// We do a simple HTTP GET to the URL.
		WWW www = IzUtils.CreateWWW (url_virtualgoods, app_auth_key);
		yield return www;
		int responseCode = getResponseCode (www);

		if (responseCode != 200) {
			
			error.success = false;
			error.error_message = "Failed to get store virtual goods.";
			yield break;
		}
		error.success = true;
		error.error_message = "";

		// Sadly, JsonUtility does not support parsing a list as a top level object. In other words, JSON like
		// [{"name1":"value1"}, {"name2":"value2"}] cannot be parsed, but JSON like:
		// {"list":[{"name1":"value1"}, {"name2":"value2"}]} can. So, we simply create that kind of an object
		// from what we received from IntraZon with crude string manipulation. Or, we could use a more heavyweight
		// JSON library, but they are also slower.
		string fakeListObject = "{\"virtual_goods\":" + www.text + "}";

		Debug.Log ("Store contents: " + fakeListObject);

		IzVirtualGoodArray goods = JsonUtility.FromJson<IzVirtualGoodArray> (fakeListObject);
		virtual_goods.Clear ();

		// We go through the list in our wrapper object, and copy each item to the list given in the parameter.
		foreach (_IzVirtualGood _good in goods.virtual_goods) {

			IzStoreVirtualGood good = new IzStoreVirtualGood();
			good.key = _good.key;
			good.name = _good.name;
			good.price = _good.price;
			good.thumbnail_url = _good.thumbnail;
			good.thumbnail = new IzThumbnail ();
			good.thumbnail.success = false;
			virtual_goods.Add (good);
		}
	}

	/// <summary>
	/// Fills the given list downloadable_goods with store contents for this app; with objects of type IzDownloadableGood.
	/// </summary>
	public static IEnumerator GetDownloadableGoods(string iz_host, string app_auth_key, string app_key, List<IzStoreDownloadableGood> downloadable_goods, 
		IzCoroutineError error) {

		string url_format_downloadablegoods = "/api/v1/applications/{0}/downloadable-goods/";
		string url_downloadablegoods = iz_host + String.Format(url_format_downloadablegoods, app_key);

		// We do a simple HTTP GET to the URL.
		WWW www = IzUtils.CreateWWW (url_downloadablegoods, app_auth_key);
		yield return www;
		int responseCode = getResponseCode (www);

		if (responseCode != 200) {

			error.success = false;
			error.error_message = "Failed to get store downloadable goods.";
			yield break;
		}
		error.success = true;
		error.error_message = "";

		// Sadly, JsonUtility does not support parsing a list as a top level object. In other words, JSON like
		// [{"name1":"value1"}, {"name2":"value2"}] cannot be parsed, but JSON like:
		// {"list":[{"name1":"value1"}, {"name2":"value2"}]} can. So, we simply create that kind of an object
		// from what we received from IntraZon with crude string manipulation. Or, we could use a more heavyweight
		// JSON library, but they are also slower.
		string fakeListObject = "{\"downloadable_goods\":" + www.text + "}";

		Debug.Log ("Store contents: " + fakeListObject);

		IzDownloadableGoodArray goods = JsonUtility.FromJson<IzDownloadableGoodArray> (fakeListObject);
		downloadable_goods.Clear ();

		// We go through the list in our wrapper object, and copy each item to the list given in the parameter.
		foreach (_IzDownloadableGood _good in goods.downloadable_goods) {
			IzStoreDownloadableGood good = new IzStoreDownloadableGood ();
			good.description = _good.description;
			good.general = _good.general;
			good.key = _good.key;
			good.name = _good.name;
			good.price = _good.price;
			good.thumbnail_url = _good.thumbnail;
			good.thumbnail = new IzThumbnail ();
			good.thumbnail.success = false;
			downloadable_goods.Add (good);
		}
	}

	/// <summary>
	/// Fills the given list virtual_goods with permanent virtual goods owned by player
	/// </summary>
	public static IEnumerator GetOwnedVirtualGoods(string iz_host, string app_auth_key, string site_key, string app_key, string player_key, 
		List<IzOwnedVirtualGood> virtual_goods, IzCoroutineError error) {

		string url_format_virtualgoods = "/api/v1/sites/{0}/applications/{1}/players/{2}/virtual-goods/";
		string url_virtualgoods = iz_host + String.Format(url_format_virtualgoods, site_key, app_key, player_key);

		// We do a simple HTTP GET to the URL.
		WWW www = IzUtils.CreateWWW (url_virtualgoods, app_auth_key);
		yield return www;
		int responseCode = getResponseCode (www);

		if (responseCode != 200) {

			error.success = false;
			error.error_message = "Failed to get owned virtual goods.";
			yield break;
		}
		error.success = true;
		error.error_message = "";

		// Sadly, JsonUtility does not support parsing a list as a top level object. In other words, JSON like
		// [{"name1":"value1"}, {"name2":"value2"}] cannot be parsed, but JSON like:
		// {"list":[{"name1":"value1"}, {"name2":"value2"}]} can. So, we simply create that kind of an object
		// from what we received from IntraZon with crude string manipulation. Or, we could use a more heavyweight
		// JSON library, but they are also slower.
		string fakeListObject = "{\"virtual_goods\":" + www.text + "}";
		Debug.Log ("List: " + fakeListObject);

		IzVirtualGoodArray goods = JsonUtility.FromJson<IzVirtualGoodArray> (fakeListObject);

		// We go through the list in our wrapper object, and copy each item to the list given in the parameter.
		foreach (_IzVirtualGood _good in goods.virtual_goods) {
			IzOwnedVirtualGood good = new IzOwnedVirtualGood ();
			good.key = _good.key;
			good.name = _good.name;
			virtual_goods.Add (good);
		}
	}

	/// <summary>
	/// Fills the given list downloadable_goods with those owned by player
	/// </summary>
	public static IEnumerator GetOwnedDownloadableGoods(string iz_host, string app_auth_key, string site_key, string app_key, string player_key, 
		List<IzOwnedDownloadableGood> downloadable_goods, IzCoroutineError error) {

		string url_format_downloadablegoods = "/api/v1/sites/{0}/applications/{1}/players/{2}/downloadable-goods/";
		string url_downloadablegoods = iz_host + String.Format(url_format_downloadablegoods, site_key, app_key, player_key);

		// We do a simple HTTP GET to the URL.
		WWW www = IzUtils.CreateWWW (url_downloadablegoods, app_auth_key);
		yield return www;
		int responseCode = getResponseCode (www);

		if (responseCode != 200) {

			error.success = false;
			error.error_message = "Failed to get owned downloadable goods.";
			yield break;
		}
		error.success = true;
		error.error_message = "";

		// Sadly, JsonUtility does not support parsing a list as a top level object. In other words, JSON like
		// [{"name1":"value1"}, {"name2":"value2"}] cannot be parsed, but JSON like:
		// {"list":[{"name1":"value1"}, {"name2":"value2"}]} can. So, we simply create that kind of an object
		// from what we received from IntraZon with crude string manipulation. Or, we could use a more heavyweight
		// JSON library, but they are also slower.
		string fakeListObject = "{\"downloadable_goods\":" + www.text + "}";
		Debug.Log ("List: " + fakeListObject);

		IzDownloadableGoodArray goods = JsonUtility.FromJson<IzDownloadableGoodArray> (fakeListObject);

		// We go through the list in our wrapper object, and copy each item to the list given in the parameter.
		foreach (_IzDownloadableGood _good in goods.downloadable_goods) {
			IzOwnedDownloadableGood good = new IzOwnedDownloadableGood ();
			good.key = _good.key;
			good.name = _good.name;
			good.url = _good.url;
			downloadable_goods.Add (good);
		}
	}

	/// <summary>
	/// Fills the given list virtual_goods with store contents for this app; with objects of type IzVirtualGood.
	/// </summary>
	public static IEnumerator GetThumbnail(string iz_host, string url, IzThumbnail thumbnail) {

		// Simple HTTP GET
		WWW www = new WWW (iz_host + url);
		yield return www;

		if (string.IsNullOrEmpty(www.error)) {
			thumbnail.texture = www.texture;
			thumbnail.success = true;
		} else {
			thumbnail.texture = null;
			thumbnail.success = false;
		}
	}

	/// <summary>
	/// Creates a new player to the given site.
	/// </summary>
	public static IEnumerator CreatePlayer(string iz_host, string site_auth_key, string site_key, string username, 
		string name = "", string email = "") {

		string url_format_createplayer_site = "/api/v1/sites/{0}/create-player/";
		string url_createplayer = iz_host + String.Format (url_format_createplayer_site, site_key);

		// We create a new player object with this data, serialize it to JSON, and HTTP POST it to the correct URL.
		_IzPlayer izPlayer = new _IzPlayer ();
		izPlayer.username = username;
		izPlayer.email = email;
		izPlayer.name = name;
		WWW www = IzUtils.CreateWWW (url_createplayer, site_auth_key, "POST", JsonUtility.ToJson (izPlayer));
		yield return www;
	}

	// This class is intended to hide all the particulars of communicating with HTTP, hence this method is private.
	private static WWW CreateWWW(string url, string auth_key = null, string method = "GET", string post_data = null) {

		if ((method == "POST" || method == "PUT" || method == "PATCH") && post_data == null) {
			// Parameters are in conflict; these methods absolutely must have data.
			return null;
		}
		WWWForm form = new WWWForm();
		System.Collections.Generic.Dictionary<string, string> headers = form.headers;

		if (method != "GET" && method != "POST") {
			// WWW object doesn't support anything other than GET and POST. This is a common workaround, which does
			// HTTP POST in reality, but the web server is instructed to look at this header instead, and determine
			// the intended method from it. Note that this requires special middleware on the server, which IntraZon
			// has.
			headers.Add("X-HTTP-Method-Override", method);
		}
		headers["Accept"] = "application/json";
		headers["Content-Type"] = "application/json";

		// HTTP Basic Auth is always done this way. Note that with our app and site keys, we use empty password. The username
		// is the secret. Nothing else IntraZon-specific on this line.
		headers["Authorization"] = 
			"Basic " + System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(auth_key + ":"));

		if (method == "GET" || method == "DELETE") {
			// These methods don't have data. WWW-class does GET, and server may interpret it as DELETE if the header
			// is present.
			return new WWW (url, null, headers);
		} else {
			// All other methods have data. WWW-class does POST, and server may interpret it as something else if the 
			// header is present.
			return new WWW (url, System.Text.ASCIIEncoding.Default.GetBytes (post_data), headers);
		}
	}
}

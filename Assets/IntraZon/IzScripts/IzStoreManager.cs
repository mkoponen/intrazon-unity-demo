﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;

public class IzStoreManager : IzSingleton<IzStoreManager> {



	protected IzStoreManager () {} // guarantee this will be always a singleton only - can't use the constructor!
	private IzPlayer izPlayer = null;
	private Dictionary<string, IzOwnedVirtualGood> izOwnedVirtualGoods = null;
	private Dictionary<string, IzOwnedDownloadableGood> izOwnedDownloadableGoods = null;
	private Dictionary<string, IzStoreVirtualGood> izStoreVirtualGoods = null;
	private Dictionary<string, IzStoreDownloadableGood> izStoreDownloadableGoods = null;

	private string iz_host = "http://devel.alpenwolf.com";
	private string app_key = "";
	private string app_auth_key = "";
	private string site_auth_key = "";
	private string site_key = "";
	private bool aborted = false;
	private bool store_aborted = false;

	void Awake () {
		DontDestroyOnLoad(transform.gameObject);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init(string app_key, string app_auth_key, string site_key, string site_auth_key) {
		
		this.app_key = app_key;
		this.app_auth_key = app_auth_key;
		this.site_key = site_key;
		this.site_auth_key = site_auth_key;
	}

	public void LoginPlayer(string username) {
		
		// New login has started. Make sure that we don't get some old values during login process if
		// GetPlayer() is called in the meanwhile.
		izPlayer = null;
		izOwnedVirtualGoods = null;
		aborted = false;

		StartCoroutine (DoLoginBasedOnUsername (username));
	}

	private IEnumerator DoLoginBasedOnUsername(string username) {

		IzPlayer player = new IzPlayer ();
		yield return IzUtils.GetPlayerWithUsername (iz_host, site_auth_key, site_key, username, player);
		yield return IzUtils.LoginPlayer (iz_host, app_auth_key, site_key, app_key, player.key);
		izPlayer = player;
	}

	public bool RequestOwnedVirtualGoods() {

		if (izPlayer == null) {
			
			Debug.LogError ("Player was not logged in while requesting owned virtual goods.");
			aborted = true;
			return false;
		}
		aborted = false;
		izOwnedVirtualGoods = null;
		StartCoroutine (DoRequestOwnedVirtualGoods ());
		return true;
	}

	public bool RequestOwnedDownloadableGoods() {

		if (izPlayer == null) {

			Debug.LogError ("Player was not logged in while requesting owned downloadable goods.");
			aborted = true;
			return false;
		}
		aborted = false;
		izOwnedDownloadableGoods = null;
		StartCoroutine (DoRequestOwnedDownloadableGoods ());
		return true;
	}

	public void RequestStoreGoods() {

		store_aborted = false;

		// These are loaded in parallel. IsStoreLoading will be false only when BOTH are done. This way we
		// don't waste time in waiting for response - the other request can process in the meanwhile.
		StartCoroutine (DoRequestStoreVirtualGoods ());
		StartCoroutine (DoRequestStoreDownloadableGoods ());
	}

	private IEnumerator DoRequestStoreVirtualGoods() {

		IzCoroutineError error = new IzCoroutineError ();
		List<IzStoreVirtualGood> virtual_goods = new List<IzStoreVirtualGood> ();
		yield return IzUtils.GetVirtualGoods(iz_host, app_auth_key, app_key, virtual_goods, error);

		if (error.success == false) {
			
			Debug.LogError ("Failed to get store contents from server.");
			store_aborted = true;
			yield break;
		}
		Dictionary<string, IzStoreVirtualGood> tmpStoreVirtualGoods = new Dictionary<string, IzStoreVirtualGood> ();

		foreach (IzStoreVirtualGood good in virtual_goods) {

			IzThumbnail izThumbnail = null;

			if (good.thumbnail_url == null || good.thumbnail_url == "") {
				
				Debug.Log ("This thumbnail was null, skipping.");

			} else {

				izThumbnail = new IzThumbnail ();
				
				yield return IzUtils.GetThumbnail (iz_host, good.thumbnail_url, izThumbnail);

				if (izThumbnail.success == false) {
				
					Debug.LogError ("Failed to load thumbnail from url " + good.thumbnail_url);
					izThumbnail = null;
				}
			}
			IzStoreVirtualGood newGood = new IzStoreVirtualGood ();
			newGood.key = good.key;
			newGood.name = good.name;
			newGood.price = good.price;
			newGood.thumbnail_url = good.thumbnail_url;
			newGood.thumbnail = izThumbnail;
			tmpStoreVirtualGoods.Add (good.key, newGood);
			
		}
		izStoreVirtualGoods = tmpStoreVirtualGoods;
		tmpStoreVirtualGoods = null;
	}

	private IEnumerator DoRequestStoreDownloadableGoods() {

		IzCoroutineError error = new IzCoroutineError ();
		List<IzStoreDownloadableGood> downloadable_goods = new List<IzStoreDownloadableGood> ();
		yield return IzUtils.GetDownloadableGoods(iz_host, app_auth_key, app_key, downloadable_goods, error);

		if (error.success == false) {

			Debug.LogError ("Failed to get downloadable store contents from server.");
			store_aborted = true;
			yield break;
		}
		Dictionary<string, IzStoreDownloadableGood> tmpStoreDownloadableGoods = new Dictionary<string, IzStoreDownloadableGood> ();

		foreach (IzStoreDownloadableGood good in downloadable_goods) {

			IzThumbnail izThumbnail = null;

			if (good.thumbnail_url == null || good.thumbnail_url == "") {

				Debug.Log ("This thumbnail was null, skipping.");

			} else {

				izThumbnail = new IzThumbnail ();

				yield return IzUtils.GetThumbnail (iz_host, good.thumbnail_url, izThumbnail);

				if (izThumbnail.success == false) {

					Debug.LogError ("Failed to load thumbnail from url " + good.thumbnail_url);
					izThumbnail = null;
				}
			}
			IzStoreDownloadableGood newGood = new IzStoreDownloadableGood ();
			newGood.key = good.key;
			newGood.name = good.name;
			newGood.price = good.price;
			newGood.thumbnail_url = good.thumbnail_url;
			newGood.description = good.description;
			newGood.general = good.general;
			newGood.thumbnail = izThumbnail;
			tmpStoreDownloadableGoods.Add (good.key, newGood);

		}
		izStoreDownloadableGoods = tmpStoreDownloadableGoods;
		tmpStoreDownloadableGoods = null;
	}

	private IEnumerator DoRequestOwnedVirtualGoods() {

		if (izPlayer == null) {
			Debug.LogError ("Player was not logged in while requesting owned virtual goods.");
			aborted = true;
			yield break;
		}
		aborted = false;
		List<IzOwnedVirtualGood> virtual_goods = new List<IzOwnedVirtualGood> ();
		IzCoroutineError error = new IzCoroutineError ();
		yield return IzUtils.GetOwnedVirtualGoods (iz_host, app_auth_key, site_key, app_key, izPlayer.key, virtual_goods, error);

		if (error.success == false) {
			
			Debug.LogError ("Failed to get owned virtual goods from server.");
			aborted = true;
			yield break;
		}
		izOwnedVirtualGoods = new Dictionary<string, IzOwnedVirtualGood> (); 

		foreach (IzOwnedVirtualGood good in virtual_goods) {
			
			IzOwnedVirtualGood newGood = new IzOwnedVirtualGood ();
			newGood.key = good.key;
			newGood.name = good.name;
			izOwnedVirtualGoods.Add (good.key, newGood);
		}
	}

	private IEnumerator DoRequestOwnedDownloadableGoods() {

		if (izPlayer == null) {
			Debug.LogError ("Player was not logged in while requesting owned downloadable goods.");
			aborted = true;
			yield break;
		}
		aborted = false;
		List<IzOwnedDownloadableGood> downloadable_goods = new List<IzOwnedDownloadableGood> ();
		IzCoroutineError error = new IzCoroutineError ();
		yield return IzUtils.GetOwnedDownloadableGoods (iz_host, app_auth_key, site_key, app_key, izPlayer.key, downloadable_goods, error);

		if (error.success == false) {

			Debug.LogError ("Failed to get owned downloadable goods from server.");
			aborted = true;
			yield break;
		}
		izOwnedDownloadableGoods = new Dictionary<string, IzOwnedDownloadableGood> (); 

		foreach (IzOwnedDownloadableGood good in downloadable_goods) {

			IzOwnedDownloadableGood newGood = new IzOwnedDownloadableGood ();
			newGood.key = good.key;
			newGood.name = good.name;
			izOwnedDownloadableGoods.Add (good.key, newGood);
		}
	}

	// Null means that login is not yet complete.
	public IzPlayer GetPlayer {
		get { return izPlayer; }
	}

	public bool IsAborted {
		get { return aborted; }
	}

	public bool IsStoreAborted {
		get { return store_aborted; }
	}

	public bool IsStoreLoading {
		get { return (izStoreVirtualGoods == null || izStoreDownloadableGoods == null); }
	}

	// Null means that request is not yet complete.
	public Dictionary<string, IzOwnedVirtualGood> GetOwnedVirtualGoods {
		get { return izOwnedVirtualGoods; }
	}

	public bool DrawStoreGrid() {
		
		GameObject panelObj = GameObject.Find ("IzProductPanel");

		if (panelObj == null) {
			
			Debug.LogError ("IzProductPanel not found - is IzStoreScene loaded?");
			return false;
		}
		if (izStoreVirtualGoods == null) {
			
			Debug.LogError ("Store not yet loaded. Watch for IsStoreLoading and call again when it is true.");
			return false;
		}
		GameObject grid = GameObject.Find ("Grid");

		foreach (KeyValuePair<string, IzStoreVirtualGood> good in izStoreVirtualGoods) {
			GameObject obj = Instantiate(Resources.Load("IntraZon/ProductPanelPrefab")) as GameObject;
			RawImage img = obj.transform.GetComponentInChildren<RawImage> ();
			var texts = obj.transform.GetComponentsInChildren<Text> ();

			foreach (Text text in texts) {
				if (text.name == "Name") {
					text.text = good.Value.name;
				} else if(text.name == "Price") {
					text.text = "" + good.Value.price + " Coins";
				}
			}
			Button btn = obj.transform.GetComponentInChildren<Button> ();
			IzPurchaseButton purchaseButton = btn.transform.GetComponent<IzPurchaseButton> ();
			purchaseButton.virtualgood_key = good.Key;

			if (good.Value.thumbnail != null) {
				img.texture = good.Value.thumbnail.texture;
			}
			img.color = Color.white;
			obj.transform.SetParent(grid.transform, false);
		}
		return true;
	}
}

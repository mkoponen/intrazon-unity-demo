﻿using UnityEngine;
using System.Collections;
using System;

[Serializable] 
public struct IzVirtualGoodArray { 
	public _IzVirtualGood[] virtual_goods; 
}

[Serializable] 
public struct IzDownloadableGoodArray { 
	public _IzDownloadableGood[] downloadable_goods; 
}

[Serializable]
public class _IzPlayer {
	
	public string username;
	public string name;
	public string email;
	public bool logged;
	public int coins;
	public string key;
	public string site;
	public bool payment_method_filed;

	public void CopyFrom(_IzPlayer other) {
		
		this.username = other.username;
		this.name = other.name;
		this.email = other.email;
		this.logged = other.logged;
		this.coins = other.coins;
		this.key = other.key;
		this.site = other.site;
		this.payment_method_filed = other.payment_method_filed;
	}
}

[Serializable]
public class _IzVirtualGood {
	
	public string name;
	public int price;
	public string thumbnail;
	public string key;
}


[Serializable]
public class _IzPurchase {
	
	public string name;
	public int price;
	public string thumbnail;
	public string key;
}

[Serializable]
public class _IzBuyCoinsCmd {
	public int coins;
}

[Serializable]
public class _IzBuyVirtualGoodCmd {
	
	public string virtual_good_key;
	public string currency;
}

[Serializable]
public class _IzBuyDownloadableGoodCmd {

	public string downloadable_good_key;
	public string currency;
}

[Serializable]
public class _IzRemoveVirtualGoodCmd {
	public string virtual_good_key;
}

[Serializable]
public class _IzCreditCard {
	
	public string cc_name;
	public int cc_exp_month;
	public int cc_exp_year;
	public string cc_num;
	public string cc_csc;
}

[Serializable]
public class _IzDownloadableGood {

	public string name;
	public int price;
	public string thumbnail;
	public string key;
	public string description;
	public bool general;
	public string url;
}

[Serializable]
public class IzServerResponse {
	
	public int code;
	public string message;
}

public class IzThumbnail {
	
	public bool success;
	public Texture2D texture;
}

public class IzCoroutineError {
	
	public bool success;
	public string error_message;
}

public class IzPlayer {

	public string username;
	public string key;
	public int coins;
	public string email;
	public bool payment_method_filed;
}

public class IzOwnedVirtualGood {
	
	public string name;
	public string key;
}

public class IzOwnedDownloadableGood {

	public string name;
	public string key;
	public string url;
}

public class IzStoreVirtualGood {

	public string name;
	public int price;
	public IzThumbnail thumbnail;
	public string thumbnail_url;
	public string key;
}

public class IzStoreDownloadableGood {

	public string name;
	public int price;
	public IzThumbnail thumbnail;
	public string thumbnail_url;
	public string key;
	public string description;
	public bool general;
}
	
public class IzCreditCard {

	public string cc_name;
	public int cc_exp_month;
	public int cc_exp_year;
	public string cc_num;
	public string cc_csc;
}
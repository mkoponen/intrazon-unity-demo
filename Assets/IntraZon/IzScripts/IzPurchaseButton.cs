﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IzPurchaseButton : MonoBehaviour {

	[SerializeField]
	public string virtualgood_key = "";
	[SerializeField] 
	private Button MyButton = null; // assign in the editor

	// Use this for initialization
	void Start () {
		
		MyButton = this.transform.GetComponent<Button> ();
		MyButton.onClick.AddListener(() => Pushed());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Pushed() {
		
		GameObject izStoreManagerObj = GameObject.Find ("IzStoreManager");

		if (izStoreManagerObj == null) {
			
			Debug.LogError ("IzStoreManager object not found; It must be instantiated before pushing purchase buttons.");
			return;
		}
		IzStoreManager izStoreManager = izStoreManagerObj.GetComponent<IzStoreManager> ();
	}
}

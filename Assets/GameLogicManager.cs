﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;

public class GameLogicManager : MonoBehaviour {
	
	string storeJson = "";
	string playersJson = "";
	string loggedPlayer = "";
	bool paymentMethodFiled = false;

	string iz_host = "http://devel.alpenwolf.com";

	// Production address commented below. Not available yet. Note https for production and http for development.
	//string iz_host = "https://iz.alpenwolf.com";

	// This identifies your application. Default string refers to Test Application, whose data
	// you should not modify. It is merely to see how the system works and should provide predictable
	// result for all Alpenwolf developers. Create your own application to Test Site, and change this
	// string to refer to its key.
	//public string app_key = "JL3Lfrt9KP";
	//public string app_auth_key = "app_O65hlZQm59SAgFmP8HgVK2VH";
	[System.NonSerialized]
	public string app_key = "CK0ikN5bdfb";
	[System.NonSerialized]
	public string app_auth_key = "app_sma9HWcoOwZlRQddK18QFMCX";
	[System.NonSerialized]
	public string site_auth_key = "sit_xWJPmeKbxpK0PH8MKjxUatf5";

	// This will stay as it is for testing.
	[System.NonSerialized]
	public string site_key = "Cy41q7gUHso";

	string pendingVirtualGood = null;
	int coins = 0;
	Text activeNotification = null;

	void Awake() {
	}
	
	// Use this for initialization
	IEnumerator Start () {
		// Below is a quick and very dirty way to create a player to the Test Site, by uncommenting and executing this
		// _once_, obviously with values of your own. First is username, second is email, third is visible name. Preferably
		// Attach this functionality to some form of your own.
		// yield return IzUtils.CreatePlayer(iz_host, site_auth_key, site_key, "mk2", "Make");

		SetCanvasActive ("login");
		GameObject panelObj = GameObject.Find ("ProductPanel");
		List<IzVirtualGood> virtual_goods = new List<IzVirtualGood> ();
		yield return IzUtils.GetVirtualGoods(iz_host, app_auth_key, app_key, virtual_goods);
		GameObject grid = GameObject.Find ("Grid");

		foreach (IzVirtualGood good in virtual_goods) {
			GameObject obj = Instantiate(Resources.Load("ProductPanelPrefab")) as GameObject;
			RawImage img = obj.transform.GetComponentInChildren<RawImage> ();
			var texts = obj.transform.GetComponentsInChildren<Text> ();
			
			foreach (Text text in texts) {
				if (text.name == "Name") {
					text.text = good.name;
				} else if(text.name == "Price") {
					text.text = "" + good.price + " Coins";
				}
			}
			Button btn = obj.transform.GetComponentInChildren<Button> ();
			PurchaseButton purchaseButton = btn.transform.GetComponent<PurchaseButton> ();
			purchaseButton.virtualgood_key = good.key;

			// Note that this is just a little bit of a shortcut, to store the coin price away so that the button
			// onclick function can access it. You could also request it again from IZ using the product key. But
			// this way makes things a bit more responsive.
			purchaseButton.coins_price = good.price;

			IzThumbnail izThumbnail = new IzThumbnail ();
			yield return IzUtils.GetThumbnail (iz_host, good.thumbnail, izThumbnail);
			img.texture = izThumbnail.texture;
			img.color = Color.white;
			obj.transform.SetParent(grid.transform, false);
		}
	}

	void SetCanvasActive(string id) {

		// Since it turned out that SimpleJSON cannot create adequately good output, credit-canvas is not
		// yet functional. It will be soon, as I change the parser to something more advanced.

		switch (id) {
		case "store":
			{
				GameObject storeCanvas = GameObject.Find ("StoreCanvas");
				storeCanvas.GetComponent<Canvas> ().sortingOrder = 0;
				GameObject creditCanvas = GameObject.Find ("CreditCanvas");
				creditCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				GameObject loginCanvas = GameObject.Find ("LoginCanvas");
				loginCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				storeCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
				storeCanvas.GetComponent<CanvasGroup> ().interactable = true;
				creditCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				creditCanvas.GetComponent<CanvasGroup> ().interactable = false;
				loginCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				loginCanvas.GetComponent<CanvasGroup> ().interactable = false;
				break;
			}	
		case "credit":
			{
				GameObject storeCanvas = GameObject.Find ("StoreCanvas");
				storeCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				GameObject creditCanvas = GameObject.Find ("CreditCanvas");
				creditCanvas.GetComponent<Canvas> ().sortingOrder = 0;
				GameObject loginCanvas = GameObject.Find ("LoginCanvas");
				loginCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				storeCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				storeCanvas.GetComponent<CanvasGroup> ().interactable = false;
				creditCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
				creditCanvas.GetComponent<CanvasGroup> ().interactable = true;
				loginCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				loginCanvas.GetComponent<CanvasGroup> ().interactable = false;
				break;
			}
		case "login":
			{
				GameObject storeCanvas = GameObject.Find ("StoreCanvas");
				storeCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				GameObject creditCanvas = GameObject.Find ("CreditCanvas");
				creditCanvas.GetComponent<Canvas> ().sortingOrder = -1;
				GameObject loginCanvas = GameObject.Find ("LoginCanvas");
				loginCanvas.GetComponent<Canvas> ().sortingOrder = 0;
				storeCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				storeCanvas.GetComponent<CanvasGroup> ().interactable = false;
				creditCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
				creditCanvas.GetComponent<CanvasGroup> ().interactable = false;
				loginCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
				loginCanvas.GetComponent<CanvasGroup> ().interactable = true;
				break;
			}
		}
	}
		
	// Update is called once per frame
	void Update () {
	}

	public void LoginPlayer() {
		
		GameObject loginFieldUsername = GameObject.Find ("LoginFieldUsername");
		string username = loginFieldUsername.transform.GetComponentInChildren<InputField> ().text;
		StartCoroutine (DoLoginBasedOnUsername (username));
	}

	public void SaveCredit () {
		// If there is something wrong with any of the fields, set an informative message to
		// the gameobject ErrorMessage, and simply return immediately.

		GameObject form = GameObject.Find ("FormCard");
		GameObject errorMessage = GameObject.Find ("ErrorMessage");
		GameObject fieldName = form.transform.Find ("FieldName").gameObject;
		GameObject fieldNum = form.transform.Find ("FieldNum").gameObject;
		GameObject fieldMonth = form.transform.Find ("FieldMonth").gameObject;
		GameObject fieldYear = form.transform.Find ("FieldYear").gameObject;
		GameObject fieldCsc = form.transform.Find ("FieldCsc").gameObject;
		string name = fieldName.transform.GetComponentInChildren<InputField> ().text;
		string num = fieldNum.transform.GetComponentInChildren<InputField> ().text;
		string month_str = fieldMonth.transform.GetComponentInChildren<InputField> ().text;
		string year_str = fieldYear.transform.GetComponentInChildren<InputField> ().text;
		string csc = fieldCsc.transform.GetComponentInChildren<InputField> ().text;
		int month;
		int year;
		if (Int32.TryParse (month_str, out month) == false) {
			errorMessage.GetComponent<Text> ().text = "Error: month must be a number";
			return;
		}
		if (Int32.TryParse (year_str, out year) == false) {
			errorMessage.GetComponent<Text> ().text = "Error: year must be a number";
			return;
		}
		IzCreditCard creditCard = new IzCreditCard ();
		creditCard.cc_name = name;
		creditCard.cc_num = num;
		creditCard.cc_exp_month = month;
		creditCard.cc_exp_year = year;
		creditCard.cc_csc = csc;

		StartCoroutine (DoSaveCredit (creditCard));
	}

	private IEnumerator DoSaveCredit (IzCreditCard creditCard) {

		IzCoroutineError error = new IzCoroutineError ();
		yield return IzUtils.FileCreditCard (iz_host, site_auth_key, site_key, loggedPlayer, creditCard, error);
		if (error.success == false) {
			Debug.LogError (error.error_message);
		} else {
			SetCanvasActive ("store");
		}
	}

	private IEnumerator DoLoginBasedOnUsername(string username) {
		
		IzPlayer player = new IzPlayer ();
		yield return IzUtils.GetPlayerWithUsername (iz_host, site_auth_key, site_key, username, player);
		loggedPlayer = player.key;
		coins = player.coins;
		paymentMethodFiled = player.payment_method_filed;
		GameObject.Find ("CoinsNum").transform.GetComponent<Text> ().text = "" + coins;
		yield return IzUtils.LoginPlayer (iz_host, app_auth_key, site_key, app_key, loggedPlayer);
		yield return DoGetOwnedVirtualGoods ();
		SetCanvasActive ("store");
	}

	private IEnumerator DoGetOwnedVirtualGoods() {

		List<IzVirtualGood> virtual_goods = new List<IzVirtualGood> ();
		yield return IzUtils.GetOwnedVirtualGoods (iz_host, app_auth_key, site_key, app_key, loggedPlayer, virtual_goods);

		foreach (IzVirtualGood good in virtual_goods) {
			Debug.Log ("Owned: " + good.name);
		}
	}

	public void BuyVirtualGood(string key, int coinsPrice) {
		
		// Note how we didn't actually need to give the coin price as parameter to this function. We could also
		// request it again from IZ using the key. We simply can save a bit of time this way, since we had the
		// cost available at the same time as we attached the key to the button. Might as well reuse it.

		int missingCoins = coinsPrice - coins;

		if (missingCoins < 0) {
			missingCoins = 0;
		}
		// Smallest number of coins a player can buy is 3. It wouldn't make financial sense to process a smaller transaction. So, IZ won't allow it.

		if (missingCoins > 0 && missingCoins < 3) {
			missingCoins = 3;
		}
		if (paymentMethodFiled == false && missingCoins > 0) {
			// This means that the player has run out of coins with this purchase, and has not at any time filed credit card info.
			// We must do it now.
			SetCanvasActive ("credit");
		} else if(missingCoins > 0) {
			StartCoroutine (DoBuyCoinsAndBuyVirtual (missingCoins, key));
		} else {
			StartCoroutine (DoBuyVirtual (key));
		}
	}

	// The difference between the four Do... and Coroutine... -functions is that the Do-functions also react to error or success and change the game state
	// accordingly. Coroutine... -functions merely execute the task, and then set the error object appropriately.

	private IEnumerator DoBuyCoinsAndBuyVirtual (int num_coins, string virtual_good_key) {
		
		IzCoroutineError error = new IzCoroutineError ();
		yield return CoroutineBuyCoins (num_coins, error);

		if (error.success == false) {
			
			Debug.LogError ("Unable to buy coins for this virtual good. Error: " + error.error_message);
			SetCanvasActive ("store");
			yield break;
		}
		yield return CoroutineBuyVirtualGood (virtual_good_key, error);

		if (error.success == false) {

			Debug.LogError ("Unable to purchase this virtual good. Error: " + error.error_message);
			SetCanvasActive ("store");
			yield break;
		}
		SetCanvasActive ("store");
	}

	private IEnumerator DoBuyVirtual (string virtual_good_key) {

		IzCoroutineError error = new IzCoroutineError ();
		yield return CoroutineBuyVirtualGood (virtual_good_key, error);

		if (error.success == false) {

			Debug.LogError ("Unable to purchase this virtual good. Error: " + error.error_message);
			SetCanvasActive ("store");
			yield break;
		}
		SetCanvasActive ("store");
	}

	private IEnumerator CoroutineBuyCoins (int num_coins, IzCoroutineError error) {
		
		IzCoroutineError error2 = new IzCoroutineError ();
		yield return IzUtils.BuyCoins (iz_host, site_auth_key, site_key, loggedPlayer, num_coins, error2);
		if (error2.success) {
			coins += num_coins;
			error.success = true;
		} else {
			error.success = false;
			error.error_message = error2.error_message;
		}
	}

	private IEnumerator CoroutineBuyVirtualGood (string virtual_good_key, IzCoroutineError error) {

		IzCoroutineError error2 = new IzCoroutineError ();
		yield return IzUtils.BuyVirtualGood (iz_host, app_auth_key, site_key, app_key, loggedPlayer, virtual_good_key, error2);
		if (error2.success) {
			error.success = true;
		} else {
			error.success = false;
			error.error_message = error2.error_message;
		}
	}

	private IEnumerator CoroutineRemoveVirtualGood (string virtual_good_key, IzCoroutineError error) {

		IzCoroutineError error2 = new IzCoroutineError ();
		yield return IzUtils.RemoveVirtualGood (iz_host, app_auth_key, site_key, app_key, loggedPlayer, virtual_good_key, error2);
		if (error2.success) {
			error.success = true;
		} else {
			error.success = false;
			error.error_message = error2.error_message;
		}
	}
}